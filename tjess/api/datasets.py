from __future__ import absolute_import, division, print_function

import numpy as np
import requests
from bson import ObjectId
from tjess.databases import Database
from tjess.databases.mongodb import MongoDB
import tjess.utils.schema as schema
from tjess.utils.security import unsign_and_decrypt, encrypt_and_sign

from gridfs import GridFS
from pymongo import MongoClient
from tjess.types import DataReference, Datum
from tjess.utils.config import Config
import tjess
import os






class Dataset:
    def __init__(self, user, dataset_json, config=None, api=True):
        """ Dataset object"""
        if config is None:
            cfg = Config(os.path.split(os.path.abspath(tjess.__file__))[0])
            cfg.from_pyfile('settings.py')
            self.config = cfg
        else:
            self.config = config
        response = dataset_json
        self.id = response['id']
        self.name = response['name']
        self.datatype = response['datatype']
        self.database_type = response['database_type']
        self.collection = response['collection']
        if api:
            self.api_access_token=response['api_access_token']
        else:
            self.api_access_token=response['app_access_token']

        self.access_token_salt=response['access_token_salt']


        self.database_name = 'tjess_'+str(user.id)

        # some function to decode the access token given the api_key
        if api:
            self.connection_string = unsign_and_decrypt(self.api_access_token, user.api_key, self.access_token_salt)
        else:
            self.connection_string = unsign_and_decrypt(self.api_access_token, user.session_token, self.access_token_salt)

        if self.config['DEBUG']:
            self.connection_string= self.config['TJESS_DB_CONNECTION_STRING']

        if self.database_type == 'MongoDB':
            self._client = MongoDB(self)




    def __str__(self):
        """ create a string representation for the Datasets object. This will be a numpy list of object ids"""
        return 'Dataset {}'.format(self.name)

    def __len__(self):
        """ return the lenght of the dataset """
        return self._client.count()

    def __getitem__(self, key):
        """ return the requested key of the dataset """
        if isinstance(key, int):
            return self.get(index=key)
        elif isinstance(key, str):
            return self.get(objectid=key)
        elif isinstance(key, ObjectId):
            return self.get(objectid=str(key))
        else:
            raise IndexError('index of type '+ str(type(key)) + ' is not accepted')

    def __setitem__(self, key, value):
        """ set item """
        pass

    ## statistic methods
    def count(self):
        return self._client.count()


    ## data methods
    def get(self, objectid=None, index=None):
        # 1. get the entry from the database
        if objectid is not None:
            entry = self._client.get(objectid=objectid)
        else:
            entry = self._client.get(index=index)
        # 2. create data reference
        datum = Datum(entry)
        datum.load()
        # 3. get data from data reference
        return datum


    def insert(self, datum):
        # 1. create data reference
        if datum.raw['data_type'] != 'structured':
            datum.ref.put()
        # 4. save entry into user database
        return self._client.insert(datum.raw)














class Datasets:
    def __init__(self, user, base_url, config):
        """ init models class """
        self.base_url = base_url
        self.datasets_url = self.base_url + '/api/datasets/'
        self.header = {'api_key':user.api_key}
        self.set = user.datasets
        self.owner = user
        self.config = config

    def __str__(self):
        """ create a string representation for the Datasets object. This will be a numpy list of object ids"""
        return self.set.__str__()

    def __len__(self):
        """ return the lenght of the dataset """
        return len(self.set)

    def __getitem__(self, key):
        """ return the requested key of the dataset """
        if isinstance(key, slice):
            # key is a slice properly handle slices here
            response=[]
            for ds in self.get_many(self.set[key]):
                response.append(Dataset(self.owner, ds['data'][0], self.config))
            return response

        elif isinstance(key, str):
            # key is string format so trying to get it by name handle properly
            idx = np.where(self.set == key)[0]
            return Dataset(self.owner, self.get(self.set[idx])['data'][0], self.config)

        elif isinstance(key, int):
            # key is index so get the corresponding dataset
            return Dataset(self.owner, self.get(self.set[key])['data'][0], self.config)

        elif isinstance(key, list) or isinstance(key, np.array):
            # key is list type so get the corresponing datasets
            resp = []
            for i in key:
                resp.append(Dataset(self.owner, self.get(self.set[i])['data'][0], self.config))
            return resp

        else:
            raise IndexError('Index is not of the following types: integer, string, list or slice')


    def get(self, dataset_name):
        """ request dataset id from tjess main app"""
        url = self.datasets_url + dataset_name
        return requests.get(url, headers=self.header).json()

    def get_many(self, dataset_ids):
        """ request dataset id from tjess main app"""
        lst = '+'.join(dataset_ids)
        url = self.datasets_url + lst
        return requests.get(url, headers=self.header).json()
