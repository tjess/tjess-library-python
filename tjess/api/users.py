from __future__ import absolute_import, division, print_function
import numpy as np
import requests
import json

class User:
    def __init__(self, api_key, base_url, config):
        """ init models class """
        self.base_url = base_url
        self.api_key = api_key
        self.header = {'api-key':self.api_key}
        self.config = config

        r = requests.get(self.base_url + '/api/accounts/me', headers=self.header)
        self.json = r.json()
        if not self.json['success']:
            if self.config['DEBUG']:
                raise Exception(self.json['error'])
            else:
                raise Exception('Could not connect to api. Do you have the proper api key?')
        else:
            self.id = self.json['data'][0]['id']
            self.username = self.json['data'][0]['username']
            self.datasets = self.json['data'][0]['datasets']
            self.models = self.json['data'][0]['models']
