# -*- coding: utf-8 -*-
"""
Created on Wed Mar 15 16:13:15 2017

@author: T.Gloudemans
"""
from __future__ import absolute_import, division, print_function

import numpy as np
import requests
import json

from tjess.api.datasets import Datasets
from tjess.api.jobs import Jobs
from tjess.api.models import Models
from tjess.api.users import User
from tjess.api.services import Service


class Client:
    def __init__(self, url, api_key):
        self.url = url
        self.api_key = api_key
        self.header = {'api_key':self.api_key}
        self.datasets=Datasets(self.url, self.header)
        self.jobs=Jobs(self.url, self.header)
        self.models=Models(self.url, self.header)
        self.services=Services(self.url, self.header)
        self.users=Users(self.url, self.header)
