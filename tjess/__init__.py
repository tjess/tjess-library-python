from __future__ import absolute_import, division, print_function
from tjess.api.users import User
from tjess.api.datasets import Datasets
import tjess.utils
from tjess.utils.config import Config
import os

class Tjess:
    def __init__(self, api_key, config=None):
        # first get the corresponding user. Raise exception when user could not be found
        if config is None:
            self.config = Config(os.path.split(os.path.abspath(__file__))[0])
            self.config.from_pyfile('settings.py')
        else:
            self.config = config

        self.user = User(api_key, self.config['TJESS_BASE_URL'], self.config)



        # We do not ini
        self.datasets = Datasets(self.user,  self.config['TJESS_BASE_URL'], self.config)
        #self.models = Datasets(self.user, tjess.config.TJESS_BASE_URL)
