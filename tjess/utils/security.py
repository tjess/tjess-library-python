from __future__ import absolute_import, division, print_function
from itsdangerous import URLSafeTimedSerializer, URLSafeSerializer

def encrypt_and_sign(var, key, salt):
    serializer = URLSafeSerializer(key)
    return serializer.dumps(var, salt=salt)


def unsign_and_decrypt(token, key, salt):
    serializer = URLSafeSerializer(key)
    try:
        var = serializer.loads( token, salt=salt)
    except:
        return False
    return var
