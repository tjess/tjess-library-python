from tjess import Tjess
import numpy as np
import cv2
from tjess.utils import schema
import matplotlib.pyplot as plt
from bson import ObjectId
from tjess.types import DataReference, Datum
from tjess.utils.config import Config
import os


cfg = Config(os.path.join(os.getcwd(), 'tjess'))
cfg.from_pyfile('settings.py')

cfg['TJESS_BASE_URL']='http://192.168.2.18:5000'
cfg['TJESS_DB_CONNECTION_STRING']='mongodb://192.168.2.18:27017'

tj = Tjess('InRlc3Qi._gYsm52SBG99GGhylnpEWTG5Das', config=cfg)


ds = tj.datasets[0]
#url='mongodb://localhost:27017/'
#dbname = 'tjess_'+str(tj.user.id)
#data_ref = schema.create_data_ref(url=url, protocol='gridfs', content_type='image/jpg', database=dbname)
#entry = schema.create_entry(ds.id, tj.user.id, ds.datatype, data_ref=data_ref, name='panda')

#with open('D:/Projects/machinelearningplatform/app/static/images/brain_gears.jpg', 'rb') as f:
#    datum = Datum(entry, blob=f.read())
#    ds.insert(datum)

print(len(ds))
dref =ds[len(ds)-1]

img = dref.get_data()

plt.imshow(img)
