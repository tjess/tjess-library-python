from __future__ import absolute_import, division, print_function
import numpy as np
from bson import ObjectId
from gridfs import GridFS
from pymongo import MongoClient
#import cv2


class Datum:
    """
    This class assumes that the blob can be loaded into memory in full.
    This practically limits the size of te file.

    For larger files use LargeDatum class
    """
    def __init__(self, entry, blob=None, structured=False):
        self.raw = entry
        self.labels = entry['labels']
        if structured:
            self.ref = None
        else:
            self.ref = DataReference(entry['data'], blob=blob)
        self.blob = blob



    def set_data(self):
        if self.raw['data_type']=='images':
            #self.data = cv2.cvtColor(cv2.imdecode(np.asarray(bytearray(self.blob), dtype=np.uint8), 1 ), cv2.COLOR_BGR2RGB)
            pass
        elif self.raw['data_type']=='video':
            # do something with video file
            pass
        elif self.raw['data_type']=='text':
            self.data = self.blob
        else:
            # do nothing
            pass

    def get_data(self):
        """ return data"""
        self.set_data()
        return self.data

    def load(self):
        self.blob = self.ref.get().read()
        self.set_data()

    def save(self):
        """ save the current blob to reference"""
        self.ref.put()
        self.raw['data']=self.ref.export()


class DataReference:
    def __init__(self, ref_dict, blob=None):
        self.raw = ref_dict
        self.blob = blob

        if ref_dict['protocol']=='gridfs':
            # the data is stored on a gridfs database
            self.url = ref_dict['url']
            self.client = MongoClient(self.url)
            self.database = ref_dict['database']
            self.db = self.client[self.database]
            self.fs = GridFS(self.db)
            self.content_type = ref_dict['content_type']
            self.id = ref_dict['id']
        elif ref_dict['protocol']=='s3':
            pass
        elif ref_dict['protocol']=='azure':
            pass
        else:
            raise Exception('Could not dereference this object.')

    def export(self):
        """ export data reference as dictionary """
        exp = self.raw
        exp['id'] = self.id
        return exp


    def put(self):
        if self.blob is not None:
            self.id = self.fs.put(self.blob, content_type=self.content_type)
            self.raw['id']=self.id
            return self.id
        else:
            raise Exception("Cannot put none type blob")

    def get(self):
        """ get blob from database. Might be good idea to make this asynchronous when multiple calls need to be made """
        if self.id is not None:
            self.blob =  self.fs.get(self.id)
            return self.blob
        else:
            raise Exception("Data reference does not contain id")
