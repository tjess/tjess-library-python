from __future__ import absolute_import, division, print_function
import numpy as np
import requests
import json




class Jobs:
    def __init__(self, base_url):
        """ init models class """
        self.base_url = base_url
        self.header = {'api_key':self.api_key}

    def get(self, model_id):
        """ request dataset id from tjess main app"""
        return requests.get(self.url, headers=self.header)

    def update(self, model_id, data):
        """ update model after training/ change"""
        return requests.post(self.url, json=json.dumps(data),headers=self.header)
