from __future__ import absolute_import, division, print_function
from pymongo import MongoClient
import tjess.utils.schema
from gridfs import GridFS
from bson import ObjectId



class MongoDB:
    def __init__(self, dataset):
        self.datasetname = dataset.name
        self.dbname = dataset.database_name
        self.colname = dataset.collection
        self.dataset = str(dataset.id)
        self.connection_string= dataset.connection_string
        self.connect()
        self.getdb()
        self.getfs()
        self.getcol()
        self.getcursor()


    def connect(self):
        self.client = MongoClient(self.connection_string)
        return self.client

    def getdb(self):
        self.db = self.client[self.dbname]
        self.fs = GridFS(self.db)
        return self.db

    def getfs(self):
        self.fs = GridFS(self.db)
        return self.fs

    def getcol(self):
        self.col = self.db[self.colname]
        return self.col

    def getcursor(self):
        self.cursor = self.col.find({'dataset':self.dataset})


    ## data methods
    def get(self, objectid=None, index=None):
        if objectid is None:
            return self.col.find({'dataset':self.dataset})[index]
        else:
            return self.col.find_one({'_id':ObjectId(objectid), 'dataset':self.dataset})

    def get_batch(self, objectids):
        return self.col.find({'_id': {'$in':objectids}, 'dataset':self.dataset})

    def insert(self, entry):
        return self.col.insert_one(entry)


    def insert_many(self, entries):
        return self.col.insert_many(entries)

    def count(self):
        return self.col.count({'dataset':self.dataset})

    def slice(self, key):
        return self.cursor[key]
