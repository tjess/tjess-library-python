from setuptools import setup, find_packages

setup(name='tjess',
      version='0.1',
      description='Data abstraction library for use in tjess.com services',
      url='https://bitbucket.org/tjess/tjess-library-python',
      author='Thijs Gloudemans',
      author_email='thijsgloudemans@live.nl',
      license='GNU GPLv3',
      packages=find_packages(),
      zip_safe=False)
