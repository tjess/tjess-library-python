# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 13:53:29 2017

@author: T.Gloudemans
"""
from __future__ import absolute_import, division, print_function
import numpy as np

def create_database_schema(title, description, collections):
    schema = {'title':title,
              'type':'database',
              'description':description,
              'collections':collections}
    return schema

def create_collection_schema(title,  description, entry_schema=None):
    schema =     {'title': title,
                  'type' : 'collection',
                  'description' : description,
                  'schema' : entry_schema
                  }
    return schema

def create_entry(datasetid, userids, data_type, data_ref=None, label_schema=[], name=None, schema=None):
    # make sure that labels will be added to array
    schema = {
               'dataset':datasetid,
               'user' : userids,
               'data_type' : data_type,
               'schema': schema,
               'name': name,
               'data' : data_ref,
               'labels' : label_schema
              }
    return schema

def create_data_ref(url=None, host=None, port=None, username=None, password=None, api_key=None, protocol=None, blob_id=None, content_type=None, database=None, collection=None):
    schema = {
           'url' : url,
           'host': host,
           'port':port,
           'database':database,
           'collection':collection,
           'username':username,
           'password':password,
           'api_key' :api_key,
           'protocol':protocol,
           'id': blob_id,
           'content_type' : content_type
           }
    return schema

def create_label(title, description, data=None):
    schema = {
                'title':title,
                'type':'label',
                'description':description,
                'shape': np.array(data).shape,
                'data': data
               }
    return schema
