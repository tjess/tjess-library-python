# -*- coding: utf-8 -*-
"""
Created on Wed Mar  8 21:03:40 2017


goal is to make data easily available trhough this library. for example an image should be available as follows

tjess.data.images.find_one(image_id)

This should be possible independent of the database behind it.


@author: T.Gloudemans
"""
from __future__ import absolute_import, division, print_function
from tjess.databases.mongodb import MongoDB
import requests
import tjess.utils.schema as schema




class Database:
    def __init__(self, dataset):
        """ Database abstraction class """
        if dataset.type == 'MongoDB':
            self._client = MongoDB(dataset)
            self._db = self._client.getdb()
            self._col= self._client.getcol()


        self.schema = self._client.schema


    def get(self, objectid):
        return self._client.get(objectid)

    def get_many(self, objectids):
        return self._client.get_batch(objectids)

    def insert(self, entry, datablob):
        return self._client.insert(entry, datablob)

    def insert_many(self, entries, datablobs):
        return self._client.insert_many(entries, datablobs)

    def count(self):
        return self._client.count()

    def slice(self, key):
        return self._client.slice(key)
